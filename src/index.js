import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';

class Sidebar extends React.Component
{
	constructor(props)
	{
		super(props);	
	}

	render()
	{
		var eles = [];

		for(var i = 0; i < this.props.imgs.length; i++)
		{
			//Use const so that the url is not removed from memory. needed for change img
			const url = this.props.imgs[i];
			eles.push(
				<div className="thumbnail">
					<img 
						alt={this.props.imgs[i]}
						src={require(this.props.imgs[i] + "")}
						onClick={() => this.props.changeImg((url))}
						/>
				</div>
			);
		}	
		
		return(
			<div className="sidebar">
				{eles}
			</div>
		);
	}
}

class Viewer extends React.Component
{
	constructor(props)
	{
		super(props);
		
		var imageArray = [
				"./img/bird.jpg",
				"./img/river.jpg",
				"./img/sun.jpg",
			];
		
		//set images and default image to first one in array
		this.state = {
			images: imageArray,
			currentSrc: imageArray[0],
		};	
	}
	
	imgChange = (src) =>
	{
		this.setState({currentSrc: src});
	};
	
	
	render()
	{		
		/* require only takes literal strings, so cannot do images[0]. instead, convert it by adding "" */
		return (
			<div>
				<Sidebar 
					imgs={this.state.images}
					changeImg={this.imgChange}/>
				<div className="imagestuff">					
					<img id="displayImg" 
						alt={this.state.images[0]}
						src={require(this.state.currentSrc + "")}/>
				</div>
			</div>
		);
	}
}

// ========================================

ReactDOM.render(
  <Viewer />,
  document.getElementById('root')
);